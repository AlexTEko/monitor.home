'use strict';

var app = angular.module('Monitor', []);

app.controller('monitorController', function($scope, $http, $interval) {

  function update() {
      $http.get("/api/get", {
      }).then(function (response) {
          //console.log(response);
          if (response) {
            $scope.hosts = response.data.hosts;
            $scope.router_nat = response.data.router_nat;
            $scope.router_dhcp = response.data.router_dhcp;
            $scope.services = response.data.services;
            $scope.config = response.data.nginx.state;
            $scope.access = response.data.nginx.access;
            $scope.error = response.data.nginx.error;
            $scope.date = response.data.date;
          }
      });
  }

  $scope.checkState = function(code) {
    switch (code) {
      case 200:
        return 'success';
        break;
      case 403:
        return 'success';
        break;
      case 401:
        return 'warning';
        break;
      case 301:
        return 'info';
        break;
      case 302:
        return 'info';
        break;
      default:
        return 'danger';
        break;
    }
  }

  $scope.checkConfig = function(rec) {
    if (rec.indexOf('ok') > 0)
      return 'success'
    if (rec.indexOf('successful') > 0)
      return 'success'
    return 'danger'
  }

  update();
  $interval(function() {update()}, 5000);
});
