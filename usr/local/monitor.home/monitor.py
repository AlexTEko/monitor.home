#!/usr/bin/env python3

from flask import Flask, request, Response, json
import time
import subprocess
import json
import datetime
import pycurl
import requests
import time
from multiprocessing import Process

app = Flask(__name__)

def get_acestream(host):
    try:
        resp = requests.get('http://{}/app/monitor'.format(host), timeout=10)
        parameters = json.loads(resp.text)
        timestamp = int(time.time())
        timediff = timestamp - parameters['last_stats_update']
        if timediff < 30:
            status = 'success'
        else:
            status = 'warning'
        acestream = {
            'name': 'acestream',
            'parameters': parameters,
            'status':status
        }
    except:
        acestream = {
            'name': 'acestream',
            'parameters': '',
            'status':'danger'
        }
    return acestream

def get_services():
    services = []
    services.append(get_acestream('acestream.home:9944'))
    return services

def get_nginx_state():
    try:
        command = "tail /var/log/nginx/error.log"
        output = subprocess.check_output(command, shell=True)
        error = output.decode()
        error = error.split('\n')
    except subprocess.CalledProcessError:
        error = []

    try:
        command = "tail /var/log/nginx/access.log -n20 | grep -v libcurl"
        output = subprocess.check_output(command, shell=True)
        access = output.decode()
        access = access.split('\n')
    except subprocess.CalledProcessError:
        access = []

    try:
        command = "nginx -t"
        output = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True)
        state = output.decode()
        state = state.split('\n')
    except subprocess.CalledProcessError:
        state = []

    state = {
        'error':error,
        'access': access,
        'state': state
    }

    return state

def get_router_nat():
    try:
        router_nat = []
        command = "ssh admin@alpha.home.tekoone.ru -p 1421 nvram get vts_rulelist"
        output = subprocess.check_output(command, shell=True)
        output = output.decode().strip()
        rows = output.split('<')
        for row in rows:
            if row == '':
                continue
            row = row.split('>')
            item = {
                'name': row[0],
                'port_ex': row[1],
                'port_in': row[3],
                'ip': row[2],
                'protocol': row[4]
            }
            router_nat.append(item)
    except:
        router_nat = []

    return router_nat

def get_router_dhcp():
    try:
        router_dhcp = []
        command = "ssh admin@alpha.home.tekoone.ru -p 1421 cat /etc/hosts.dnsmasq"
        output = subprocess.check_output(command, shell=True)
        output = output.decode().strip()
        rows = output.split('\n')
        for row in rows:
            row = row.split(' ')
            item = {
                'name': row[1],
                'ip': row[0],
            }
            router_dhcp.append(item)
    except:
        router_dhcp = []

    return router_dhcp


def get_host_proxy(host):
    proxys = []
    try:
        command = "cat /etc/nginx/sites/" + host[:-11] + " | grep proxy_pass"
        output = subprocess.check_output(command, shell=True)
        if len(output.decode())>0:
            hosts = output.decode().split('\n')
            for host in hosts:
                if host == '':
                    continue
                proxys.append(host.strip()[11:-1])
        return proxys
    except subprocess.CalledProcessError:
        return ['direct']

def get_host_status(host):
    try:
        headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
        res = requests.get('http://' + host, headers=headers)

        status = {
            'connect_time': res.elapsed.total_seconds(),
            'http_code': res.status_code
        }
    except:
        status = {
            'connect_time': 0,
            'http_code': 404
        }
    return status

def run():
    update = {
        'hosts':[],
        'date': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    }

    try:
        command = "cat /etc/nginx/sites/* | grep server_name | grep -v '~' | grep -v '#'"
        output = subprocess.check_output(command, shell=True)
        hosts = output.decode()
        hosts = hosts.split('\n')
        for host in hosts:
            host = host.strip()[12:][:-1]
            if host == '_':
                continue
            if host == '':
                continue
            host = {
                'name': host,
                'proxy':get_host_proxy(host),
                'status':get_host_status(host),
            }
            update['hosts'].append(host)
    except:
        update['hosts'] = []

    update['nginx'] = get_nginx_state()
    update['router_nat'] = get_router_nat()
    update['router_dhcp'] = get_router_dhcp()
    update['services'] = get_services()
    return json.dumps(update)

def update():
    while True:
        data = run()
        with open('/tmp/monitor.cache.dat','w') as cache:
            cache.write(data)
            cache.close()
        time.sleep(10)

@app.route("/get",methods=['GET'])
def get():
    with open('/tmp/monitor.cache.dat','r') as cache:
        data = cache.read()
        cache.close()
    return data

if __name__ == "__main__":
    p = Process(target=update)
    p.start()
    app.run(host='0.0.0.0', port='4530')
    p.terminate()
